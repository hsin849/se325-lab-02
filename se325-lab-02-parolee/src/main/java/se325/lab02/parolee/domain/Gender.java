package se325.lab02.parolee.domain;

/**
 * Simple enumeration for representing Gender.
 *
 */
public enum Gender {
	MALE, FEMALE, OTHER;
}
